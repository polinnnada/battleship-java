package org.scrum.psd.battleship.ascii;

import com.diogonunes.jcdp.color.ColoredPrinter;
import com.diogonunes.jcdp.color.api.Ansi;
import org.scrum.psd.battleship.controller.GameController;
import org.scrum.psd.battleship.controller.dto.Letter;
import org.scrum.psd.battleship.controller.dto.Position;
import org.scrum.psd.battleship.controller.dto.Ship;

import java.util.List;
import java.util.Random;
import java.util.Scanner;

import static com.diogonunes.jcdp.color.api.Ansi.Attribute.BOLD;
import static com.diogonunes.jcdp.color.api.Ansi.Attribute.CLEAR;
import static com.diogonunes.jcdp.color.api.Ansi.BColor.WHITE;
import static com.diogonunes.jcdp.color.api.Ansi.FColor.*;

public class Main {
    private final static int ROWS_COUNT = 8;
    private final static int COLUMNS_COUNT = 8;
    private static List<Ship> myFleet;
    private static List<Ship> enemyFleet;
    private static ColoredPrinter console = new ColoredPrinter.Builder(1, false).build();;

    private static void setConsoleRed() {
        console.setForegroundColor(RED);
        console.setBackgroundColor(WHITE);
    }

    private static void setConsoleGreen() {

        console.setForegroundColor(GREEN);
        console.setBackgroundColor(WHITE);
    }

    private static void setConsoleBlack() {
        console.setForegroundColor(BLACK);
        console.setBackgroundColor(WHITE);
    }

    private static void setConsoleWhite() {
        console.setForegroundColor(Ansi.FColor.WHITE);
        console.setBackgroundColor(Ansi.BColor.BLACK);
    }

    public static void main(String[] args) {
        console.setAttribute(BOLD);

        setConsoleBlack();
        console.println("                                     |__");
        console.println("                                     |\\/");
        console.println("                                     ---");
        console.println("                                     / | [");
        console.println("                              !      | |||");
        console.println("                            _/|     _/|-++'");
        console.println("                        +  +--|    |--|--|_ |-");
        console.println("                     { /|__|  |/\\__|  |--- |||__/");
        console.println("                    +---------------___[}-_===_.'____                 /\\");
        console.println("                ____`-' ||___-{]_| _[}-  |     |_[___\\==--            \\/   _");
        console.println(" __..._____--==/___]_|__|_____________________________[___\\==--____,------' .7");
        console.println("|                        Welcome to Battleship                         BB-61/");
        console.println(" \\_________________________________________________________________________|");
        console.println("");
        console.clear();

        InitializeGame();

        StartGame();
    }

    private static void StartGame() {
        Scanner scanner = new Scanner(System.in);

        setConsoleGreen();
        console.print("\033[2J\033[;H");
        console.println("                  __");
        console.println("                 /  \\");
        console.println("           .-.  |    |");
        console.println("   *    _.-'  \\  \\__/");
        console.println("    \\.-'       \\");
        console.println("   /          _/");
        console.println("  |      _  /\" \"");
        console.println("  |     /_'");
        console.println("   \\    \\_/");
        console.println("    \" \"\" \"\" \"\" \"");

        int myFleetSize = 0;
        for (Ship ship : myFleet) {
            myFleetSize += ship.getSize();
        }

        int enemyFleetSize = 0;
        for (Ship ship : enemyFleet) {
            enemyFleetSize += ship.getSize();
        }

        int countMyFleetHits = 0;
        int countEnemyFleetHits = 0;
        boolean isGameEnded = false;

        do {
            Position position = null;
            while (position == null) {
                setConsoleWhite();
                console.println("");
                console.println("Player, it's your turn");
                console.println("Enter coordinates for your shot :");
                position = parsePosition(scanner.next());
            }

            boolean isHit = GameController.checkIsHit(enemyFleet, position);
            if (isHit) {
                countEnemyFleetHits++;
                isGameEnded = (countMyFleetHits == myFleetSize) || (countEnemyFleetHits == enemyFleetSize);
                beep();
                setConsoleRed();
                console.println("                \\         .  ./");
                console.println("              \\      .:\" \";'.:..\" \"   /");
                console.println("                  (M^^.^~~:.'\" \").");
                console.println("            -   (/  .    . . \\ \\)  -");
                console.println("               ((| :. ~ ^  :. .|))");
                console.println("            -   (\\- |  \\ /  |  /)  -");
                console.println("                 -\\  \\     /  /-");
                console.println("                   \\  \\   /  /");
            }

            if (isHit) {
                console.println("Yeah ! Nice hit !", CLEAR, RED, WHITE);
            } else {
                console.println("Miss ! =(", CLEAR, BLUE, WHITE);
            }

            if (!isGameEnded) {
                position = getRandomPosition();
                isHit = GameController.checkIsHit(myFleet, position);
                console.println("");
                if (isHit) {
                    countMyFleetHits++;
                    isGameEnded = (countMyFleetHits == myFleetSize) || (countEnemyFleetHits == enemyFleetSize);
                    console.println(String.format("Computer shoot in %s%s and %s", position.getColumn(), position.getRow(), "hit your ship !"), CLEAR, RED, WHITE);
                    beep();

                    console.println("                \\         .  ./");
                    console.println("              \\      .:\" \";'.:..\" \"   /");
                    console.println("                  (M^^.^~~:.'\" \").");
                    console.println("            -   (/  .    . . \\ \\)  -");
                    console.println("               ((| :. ~ ^  :. .|))");
                    console.println("            -   (\\- |  \\ /  |  /)  -");
                    console.println("                 -\\  \\     /  /-");
                    console.println("                   \\  \\   /  /");
                } else {
                    console.println(String.format("Computer shoot in %s%s and %s", position.getColumn(), position.getRow(), "miss"), CLEAR, BLUE, WHITE);
                }
            }
        } while (!isGameEnded);
        if (countEnemyFleetHits == myFleetSize) {
            setConsoleGreen();
            console.println("CONGRATULATIONS! YOU WIN GAME!");
        } else {
            setConsoleRed();
            console.println("OH! YOU LOSE GAME! =(");
        }
    }

    private static void beep() {
        console.print("\007");
    }

    protected static Position parsePosition(String input) {
        setConsoleWhite();
        try {
            String column = input.toUpperCase().substring(0, 1);
            Letter letter = Letter.valueOf(column);

            if (letter.ordinal() + 1 > ROWS_COUNT) {
                setConsoleRed();
                console.println(String.format("Out of field with column =  %s! We have field %s:%s!", column, COLUMNS_COUNT, ROWS_COUNT));
                throw new IllegalArgumentException();
            }

            int number = Integer.parseInt(input.substring(1));
            if (number < 1 || number > ROWS_COUNT) {
                setConsoleRed();
                console.println(String.format("Out of field with row =  %d! We have field %s:%s!", number, COLUMNS_COUNT, ROWS_COUNT));
                throw new IllegalArgumentException();
            }
            return new Position(letter, number);
        } catch (Exception e) {
            setConsoleRed();
            console.println(String.format("Wrong coordinate: %s!", input));
            return null;
        }
    }

    private static Position getRandomPosition() {
        Random random = new Random();
        Letter letter = Letter.values()[random.nextInt(COLUMNS_COUNT)];
        int number = random.nextInt(ROWS_COUNT);
        return new Position(letter, number);
    }

    private static void InitializeGame() {
        InitializeMyFleet();

        InitializeEnemyFleet();
    }

    private static void InitializeMyFleet() {
        setConsoleWhite();
        Scanner scanner = new Scanner(System.in);
        myFleet = GameController.initializeShips();

        console.setAttribute(CLEAR);
        console.println("Please position your fleet (Game board has size from A to H and 1 to 8) :");

        for (Ship ship : myFleet) {
            console.println("");
            console.println(String.format("Please enter the positions for the %s (size: %s)", ship.getName(), ship.getSize()));
            for (int i = 1; i <= ship.getSize(); i++) {
                setConsoleWhite();
                console.println(String.format("Enter position %s of %s (i.e A3):", i, ship.getSize()));

                String positionInput = scanner.next();
                Position position = parsePosition(positionInput);
                // wrong coordinate
                if (position == null) {
                    i--;
                    continue;
                }

                if (!GameController.checkIsHit(myFleet, position)) {
                    ship.addPosition(position);
                } else {
                    setConsoleRed();
                    console.println(String.format("Wrong position, Enter another position %s of %s (i.e A3):", i, ship.getSize()));
                    i--;
                }
            }
        }
    }

    private static void InitializeEnemyFleet() {
        enemyFleet = GameController.initializeShips();

        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 4));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 5));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 6));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 7));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 8));

        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 5));
        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 6));
        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 7));
        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 8));

        enemyFleet.get(2).getPositions().add(new Position(Letter.A, 3));
        enemyFleet.get(2).getPositions().add(new Position(Letter.B, 3));
        enemyFleet.get(2).getPositions().add(new Position(Letter.C, 3));

        enemyFleet.get(3).getPositions().add(new Position(Letter.F, 8));
        enemyFleet.get(3).getPositions().add(new Position(Letter.G, 8));
        enemyFleet.get(3).getPositions().add(new Position(Letter.H, 8));

        enemyFleet.get(4).getPositions().add(new Position(Letter.C, 5));
        enemyFleet.get(4).getPositions().add(new Position(Letter.C, 6));
    }
}
